# PIA PRODUCTION - HOSTING - DOCKER PHP

## Builder les images

Lancer le job manuel de la version indiqué sur la Gitlab pour rebuilder la version avec les potentiels changements qui ont été commités entre-temps.

## Faire une nouvelle version

Faire un nouveau dossier et ajouter le job correspondant dans .gitlab-ci

## REGISTRY URLS

| Version | URL                                                       |
|---------|-----------------------------------------------------------|
| Latest  | registry.gitlab.com/pia-production/hosting/docker-php     |
| 8.2     | registry.gitlab.com/pia-production/hosting/docker-php:8.2 |
| 8.1     | registry.gitlab.com/pia-production/hosting/docker-php:8.1 |
